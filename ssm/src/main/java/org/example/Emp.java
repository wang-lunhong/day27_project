package org.example;

public class Emp {
    private Integer id;
    private String empName;
    private Integer empAge;
    private String sex;
    private String phone;
    private String address;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public Integer getEmpAge() {
        return empAge;
    }

    public void setEmpAge(Integer empAge) {
        this.empAge = empAge;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Emp(Integer id, String empName, Integer empAge, String sex, String phone) {
        this.id = id;
        this.empName = empName;
        this.empAge = empAge;
        this.sex = sex;
        this.phone = phone;
    }

    public void work(){
        System.out.println(this.getEmpName() + " is working.");
    }

    public void getPhoneNo(){
        System.out.println("employee "+ this.getEmpName()+"'s phone NO. is ： " +this.getPhone());
    }

    @Override
    public String toString() {
        return "Emp{" +
                "id=" + id +
                ", empName='" + empName + '\'' +
                ", empAge=" + empAge +
                ", sex='" + sex + '\'' +
                ", phone='" + phone + '\'' +
                '}';
    }
}
