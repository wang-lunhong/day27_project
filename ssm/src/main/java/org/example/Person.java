package org.example;

public class Person {
    private String name;
    private Integer age;
    private String sex;
    private String job;
    private String address;

    public Person(String name, Integer age, String sex, String job, String address) {
        this.name = name;
        this.age = age;
        this.sex = sex;
        this.job = job;
        this.address = address;
    }

    public void Sleep(String name){
        System.out.println(name+"...sleep.....");
    }
    public void Eat(String name){
        System.out.println(name+"..... eating......");
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", sex='" + sex + '\'' +
                ", job='" + job + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}
