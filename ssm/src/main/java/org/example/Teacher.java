package org.example;

/**
 * @ClassName: Teacher
 * @Description:
 * @Author: 85351
 * @ADate: 2021/10/19 22:00
 */
public class Teacher {
    private Integer id;
    private String name;
    private String age;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Teacher{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age='" + age + '\'' +
                '}';
    }

    public void printTeacher1(){
        System.out.println("我是数学老师...");
    }

    public void printTeacher2(){
        System.out.println("我是语文老师...");
    }

    public void printTeacher3(){
        System.out.println("我是英语老师...");
    }
}
