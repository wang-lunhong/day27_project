package org.example;

import org.junit.Test;

/**
 * @ClassName: UserTest
 * @Description:
 * @Author: 85351
 * @ADate: 2021/10/19 20:44
 */
public class UserTest {
    @Test
    public void PrintUser(){
        User user = new User();
        user.setId(1);
        user.setName("张三");
        user.setPassword("helloPassword");
        user.setEmail("hello@java.com");
        user.setPhone("18190756324");
        System.out.println(user);

        user.printAdmin();
        user.printUser();
    }
}
