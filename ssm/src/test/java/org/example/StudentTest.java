package org.example;

import org.junit.Test;

/**
 * @Author: Shen Qian
 * @Date : 19/10/2021
 * @Description: 学生测试类
 */
public class StudentTest {
    @Test
    public void studentTest()
    {
        Student student=new Student();

        student.setStudent_id("001");
        student.setStudent_name("Lucy");
        student.setAge(18);
        student.setSex("女");
        student.setGrade("高三一班");

        System.out.println(student.toString());

        student.reading();
        student.writing();

    }
}
